# Demoquiz - learning project

A new Flutter sandbox project to learn fast !

> *DISCLAIMER!* The git commits here are not made to review the code ! This project was made to learn fast so I didn't respect "gitflow" or other git conventions.
> My real Flutter App Project respect "gitflow" convention.

**Everything said, The minimum guide to see how and where I learned Flutter stuff just below**

## Learning sessions guide 💡

### On master branch ⬇️

Master branch is my basic flutter stuff sandbox :

- Widgets
- Platform Checking
- Material App & Scaffold
- Single Widget Layout
- Box Decoration
- Text
- Gestures
- Flex Layout
- Stack
- Scroll

### On flutter-concept branch ⬇️

Flutter-concept branch is my flutter concept sandbox :

- Theming
- Routes
- Firebase w/ flutter

### On rough-draft-quizz-app ⬇️

This branch is made to use what I learned in a real app with a real project organization, db (firebase) using provider.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.



